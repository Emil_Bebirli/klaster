import java.util.Scanner;

public class CycleFirst {
    public static void main(String[] args) {
        System.out.print("Введите размер массива: ");
        Scanner scanJ = new Scanner(System.in);
        int v = scanJ.nextInt();
        boolean f = true;
        int[] myArray = new int[v];
        for (int i = 0; i <= myArray.length - 1; i++) {
            System.out.print("Массив[" + i + "] = ");
            Scanner scanArray = new Scanner(System.in);
            myArray[i] = scanArray.nextInt();
            switch (myArray[i]) {
                case 1, 2, 3 -> f = false;//x, y, z
            }
        }
        if (!f) {
            System.out.print("Данное значение имеется в константах ");
        }
    }
}
