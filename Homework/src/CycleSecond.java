import java.util.Scanner;

public class CycleSecond {
    public static void main(String[] args) {
        System.out.print("Введите число: ");
        Scanner scanN = new Scanner(System.in);
        int n = scanN.nextInt();
        if (n > 0) {
            int t = 0;
            for (int i = 1; i < n; i = i + 2) { // n не было включено в сумму,так как в задание не было написано включительно
                t += i;
            }
            System.out.println("Сумма нечетных чисел = " + t);
        } else {
            System.out.println("Введено отрицательное число или 0");
        }
    }
}

